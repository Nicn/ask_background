package cn.source.framework.web.service;

import cn.source.common.constant.Constants;
import cn.source.common.core.domain.AjaxResult;
import cn.source.common.core.domain.entity.SysUser;
import cn.source.common.core.domain.model.LoginUser;
import cn.source.common.core.redis.RedisCache;
import cn.source.common.exception.ServiceException;
import cn.source.common.exception.user.CaptchaException;
import cn.source.common.exception.user.CaptchaExpireException;
import cn.source.common.exception.user.UserPasswordNotMatchException;
import cn.source.common.utils.*;
import cn.source.common.utils.ip.IpUtils;
import cn.source.framework.manager.AsyncManager;
import cn.source.framework.manager.factory.AsyncFactory;
import cn.source.system.service.ISysConfigService;
import cn.source.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 登录校验方法
 *
 * @author ruoyi
 */
@Component
public class SysLoginService {
    @Autowired
    private TokenService tokenService;

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysConfigService configService;

    /**
     * 三方登录验证
     *
     * @param username 用户名
     * @param password 密码
     * @return 结果
     */
    public AjaxResult thirdLogin(String username, String password) {
        SysUser user = userService.selectUserByUserName(username);
        if (user != null) {
            AjaxResult ajax = AjaxResult.success();
            // 用户验证
            Authentication authentication = null;
            try {
                // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
                authentication = authenticationManager
                        .authenticate(new UsernamePasswordAuthenticationToken(username, password));
            } catch (Exception e) {
                if (e instanceof BadCredentialsException) {
                    AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
                    throw new UserPasswordNotMatchException();
                } else {
                    AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, e.getMessage()));
                    throw new ServiceException(e.getMessage());
                }
            }
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
            LoginUser loginUser = (LoginUser) authentication.getPrincipal();
            recordLoginInfo(loginUser.getUserId());
            // 生成token
            String token = tokenService.createToken(loginUser);
            ajax.put(Constants.TOKEN, token);
            ajax.put("loginUser", loginUser);
            return ajax;
        } else {
            AjaxResult ajax = AjaxResult.success();
            String msg = "登录成功";
            if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
                msg = "用户名/密码不能为空";
                return ajax.error();
            }
            // 首先验证验证码是否正确
//        if(redisCache.getCacheObject(loginBody.getUsername()) == null || !redisCache.getCacheObject(loginBody.getUsername()).equals(loginBody.getCode())){
//            msg = "验证码过期/错误";
//            return error(msg);
//        }
            // 验证码正确则判断是否为新用户
            SysUser sysUser = userService.selectUserByUserName(username);
            // 不是新用户，创建用户
            if (sysUser == null) {
                sysUser = new SysUser();
                sysUser.setUserName(username);
                sysUser.setNickName(username);
                sysUser.setPassword(password);
                sysUser.setPhonenumber(username);
                sysUser.setPassword(SecurityUtils.encryptPassword(username));
                sysUser.setDeptId(110L);
                Long[] postIds = new Long[1];
                postIds[0] = 8L;
                Long[] roleIds = new Long[1];
                roleIds[0] = 5L;
                sysUser.setPostIds(postIds);
                sysUser.setRoleIds(roleIds);
                //  保存完用户后，还需要设置用户的角色，部门与岗位
                // userService.registerUser(sysUser);
                userService.insertUser(sysUser);
            }
            // 生成token
            LoginUser loginUser = new LoginUser(sysUser, null);
            String token = tokenService.createToken(loginUser);
            ajax.put(Constants.TOKEN, token);
            ajax.put("loginUser", loginUser);
            return ajax;
        }
    }

    /**
     * 登录验证
     *
     * @param username 用户名
     * @param password 密码
     * @param code     验证码
     * @param uuid     唯一标识
     * @return 结果
     */
    public String login(String username, String password, String code, String uuid) {
        boolean captchaOnOff = configService.selectCaptchaOnOff();
        // 验证码开关
        if (captchaOnOff) {
            validateCaptcha(username, code, uuid);
        }
        // 用户验证
        Authentication authentication = null;
        try {
            // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
            authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (Exception e) {
            if (e instanceof BadCredentialsException) {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
                throw new UserPasswordNotMatchException();
            } else {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, e.getMessage()));
                throw new ServiceException(e.getMessage());
            }
        }
        AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();
        recordLoginInfo(loginUser.getUserId());
        // 生成token
        return tokenService.createToken(loginUser);
    }

    /**
     * 校验验证码
     *
     * @param username 用户名
     * @param code     验证码
     * @param uuid     唯一标识
     * @return 结果
     */
    public void validateCaptcha(String username, String code, String uuid) {
        String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;
        String captcha = redisCache.getCacheObject(verifyKey);
        redisCache.deleteObject(verifyKey);
        if (captcha == null) {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.expire")));
            throw new CaptchaExpireException();
        }
        if (!code.equalsIgnoreCase(captcha)) {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.error")));
            throw new CaptchaException();
        }
    }

    /**
     * 记录登录信息
     *
     * @param userId 用户ID
     */
    public void recordLoginInfo(Long userId) {
        SysUser sysUser = new SysUser();
        sysUser.setUserId(userId);
        sysUser.setLoginIp(IpUtils.getIpAddr(ServletUtils.getRequest()));
        sysUser.setLoginDate(DateUtils.getNowDate());
        userService.updateUserProfile(sysUser);
    }
}
