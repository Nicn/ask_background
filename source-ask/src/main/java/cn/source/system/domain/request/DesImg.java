package cn.source.system.domain.request;

import lombok.Data;

@Data
public class DesImg {
    private String name="";
    private String url="";
}
