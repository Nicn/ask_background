package cn.source.system.domain.request;

import lombok.Data;

@Data
public class OpenTime {
    private String name="";
    private String time="";
}
