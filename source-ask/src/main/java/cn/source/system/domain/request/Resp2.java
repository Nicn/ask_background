package cn.source.system.domain.request;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * 购物会员
 * Created by macro on 2021/10/12.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Resp2 {
    @ExcelProperty("序号")
    public String id;
    @ExcelProperty("基地名称")
    public String name="";
    @ExcelProperty("类型")
    public String type="";
    @ExcelProperty("定位")
    public String position="";
    @ExcelProperty("地址")
    public String address="";
    @ExcelProperty("联系方式")
    public String tel="";
    @ExcelProperty("参观是否需要预约")
    public String orderFlag="";
    @ExcelProperty("是否需要门票")
    public String ticketFlag="否";
    @ExcelProperty("门票价格")
    public String ticketPrice="";
    @ExcelProperty("开放时间")
    public String openTime="";
    @ExcelProperty("网站链接")
    public String url="";
    @ExcelProperty("公众号二维码")
    public String wxNumber="";
    @ExcelProperty("简介")
    public String info="";

    @ExcelProperty("特色展品介绍")
    public String exhibitsDes="";
    @ExcelProperty("特色活动介绍")
    public String activityDes="";
    /**
     * 特色展品介绍图片/视频
     * */
    public List<String> exhibitsImgs=new ArrayList<>();
    /**
     * 特色活动图片/视频
     * */
    public List<String> activityImgs=new ArrayList<>();
    /**
     * 基地相关图片
     * */
    public List<String> imgs=new ArrayList<>();

    /**
     * 宣传视频
     * */
    public List<String> videos;


}
