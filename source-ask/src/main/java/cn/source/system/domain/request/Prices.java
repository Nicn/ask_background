package cn.source.system.domain.request;

import lombok.Data;

@Data
public class Prices {
    private String type="";
    private String price="";
    private String des="";
}
