package cn.source.system.domain.entity;

import cn.source.common.annotation.Excel;
import cn.source.common.core.domain.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 友情链接对象 cms_link
 *
 * @author 詹Sir
 * @date 2022-03-12
 */
@Data
public class Score
{
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    private Long userId;

    private String userName;

    private Integer score;

    private String type;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;




}
