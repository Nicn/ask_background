package cn.source.system.domain.request;

import lombok.Data;

@Data
public class AnswerDTO {

    private Integer id;

    private String title;

    private String answer;

    private String answerResult;

    private String answerType;
}
