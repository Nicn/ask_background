package cn.source.system.domain.request;

import lombok.Data;

import java.util.List;

@Data
public class AskDTO {

    private Integer id;

    private String title;

//    private String problemType;
    private String type;

    private String answer;

    private String answerType;

//    private List<AskSelect> children;
    private List<AskSelect> answerSheet;
}
