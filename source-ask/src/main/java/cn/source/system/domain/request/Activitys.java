package cn.source.system.domain.request;

import lombok.Data;

@Data
public class Activitys {

    private String name="";
    private String url="";
    private String des="";

}
