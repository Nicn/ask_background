package cn.source.system.domain.request;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * 购物会员
 * Created by macro on 2021/10/12.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Member {

    @ExcelProperty("基地名称")
    private String name;
    @ExcelProperty("坐标")
    private String position="";
    private String des="";
    private String address="";
    private String tel="";
    private String url="";
    private String wxNumber="";
    private String subscribe="";
    private String video="";
    private List<DesImg> desImg=new ArrayList<>();
    private List<OpenTime> openTime=new ArrayList<>();
    private List<Prices> prices=new ArrayList<>();
    private List<DesImg> exhibits=new ArrayList<>();
    private List<Activitys> activitys=new ArrayList<>();
    private Integer type;

    @Override
    public String toString() {
        return "Member{" +
                "name='" + name + '\'' +
                ", position='" + position + '\'' +
                ", des='" + des + '\'' +
                ", address='" + address + '\'' +
                ", tel='" + tel + '\'' +
                ", url='" + url + '\'' +
                ", wxNumber='" + wxNumber + '\'' +
                ", subscribe='" + subscribe + '\'' +
                ", video='" + video + '\'' +
                ", desImg=" + desImg +
                ", openTime=" + openTime +
                ", prices=" + prices +
                ", exhibits=" + exhibits +
                ", activitys=" + activitys +
                ", type=" + type +
                '}';
    }
}
