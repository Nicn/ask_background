package cn.source.system.domain.entity;

import cn.source.common.annotation.Excel;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

/**
 * 友情链接对象 cms_link
 *
 * @author 詹Sir
 * @date 2022-03-12
 */
@Data
public class Subject
{
    private static final long serialVersionUID = 1L;

    /** id */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /** 题号 */
    @ExcelProperty("题号")
    @Excel(name = "题号")
    private String subjectNo;

    /** 题目 */
    @ExcelProperty("题目")
    @Excel(name = "题目")
    private String title;

    /** A */
    @ExcelProperty("选项A")
    @Excel(name = "选项A")
    private String optionA;

    /** B */
    @ExcelProperty("选项B")
    @Excel(name = "选项B")
    private String optionB;


    /** C */
    @ExcelProperty("选项C")
    @Excel(name = "选项C")
    private String optionC;

    /** D */
    @ExcelProperty("选项D")
    @Excel(name = "选项D")
    private String optionD;

    /** 答案 */
    @ExcelProperty("答案")
    @Excel(name = "答案")
    private String answer;

    /** 题目类型 */
    @ExcelProperty("类型")
    @Excel(name = "类型")
    private String type;

    private Date createTime;



}
