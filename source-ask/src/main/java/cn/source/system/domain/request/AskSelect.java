package cn.source.system.domain.request;

import lombok.Data;

@Data
public class AskSelect {

//    private  String alias;
//
//    private String answer;
//
//    private Integer isSelect=0;

    private String name;

    private String value;
}
