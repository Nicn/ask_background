package cn.source.system.controller;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.json.JSONUtil;
import cn.source.common.core.controller.BaseController;
import cn.source.common.core.domain.AjaxResult;
import cn.source.common.core.domain.model.LoginUser;
import cn.source.common.utils.SecurityUtils;
import cn.source.common.utils.StringUtils;
import cn.source.common.utils.bean.BeanUtils;
import cn.source.system.domain.entity.Score;
import cn.source.system.domain.entity.Subject;
import cn.source.system.domain.request.*;
import cn.source.system.service.IScoreService;
import cn.source.system.service.ISubjectService;
import com.alibaba.excel.EasyExcel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: 问答 api控制类
 * @author: 詹Sir
 */
@RestController
@RequestMapping("/ask")
public class AskApiController extends BaseController {

    @Autowired
    private ISubjectService subjectService;

    @Autowired
    private IScoreService scoreService;

    /**
     * @Description: 查询题目集
     */
    @GetMapping("/getList")
    public AjaxResult getList(@RequestParam String type)
    {
//        LoginUser loginUser = SecurityUtils.getLoginUser();
        List<Subject> list = subjectService.lambdaQuery().groupBy(Subject::getType).list();
        List<String> list2 = list.stream().collect(Collectors.groupingBy(Subject::getType)).keySet().stream().collect(Collectors.toList());
        if(CollectionUtil.isEmpty(list2)){
            return AjaxResult.success();
        }
        List<Subject> subjects=null;
        if(StringUtils.isEmpty(type)){
            Integer num = new Random().nextInt(list2.size()-1) +1;
             subjects= subjectService.lambdaQuery().eq(Subject::getType,list2.get(num)).list();
        }else{
            subjects= subjectService.lambdaQuery().eq(Subject::getType,type).list();
        }
        if(CollectionUtil.isEmpty(subjects)){
            return AjaxResult.success();
        }else if(subjects.size()>5){
            subjects=subjects.subList(0, 5);
        }
        Collections.shuffle(subjects);
        List<AskDTO> askDTOS=new ArrayList<>();
        Integer index=1;
        for (Subject subject : subjects) {
            AskDTO askDTO=new AskDTO();
            askDTO.setId(index++);
            askDTO.setType("1");
            askDTO.setTitle(subject.getTitle());
            askDTO.setAnswer(subject.getAnswer());
            askDTO.setAnswerType(subject.getType());
            List<AskSelect> askSelects=new ArrayList<>();
            if (StringUtils.isNotEmpty(subject.getOptionA())){
                AskSelect askSelect=new AskSelect();
                askSelect.setValue("A");
                askSelect.setName(subject.getOptionA());
                askSelects.add(askSelect);
            }
            if (StringUtils.isNotEmpty(subject.getOptionB())){
                AskSelect askSelect=new AskSelect();
                askSelect.setValue("B");
                askSelect.setName(subject.getOptionB());
                askSelects.add(askSelect);
            }
            if (StringUtils.isNotEmpty(subject.getOptionC())){
                AskSelect askSelect=new AskSelect();
                askSelect.setValue("C");
                askSelect.setName(subject.getOptionC());
                askSelects.add(askSelect);
            }
            if (StringUtils.isNotEmpty(subject.getOptionD())){
                AskSelect askSelect=new AskSelect();
                askSelect.setValue("D");
                askSelect.setName(subject.getOptionD());
                askSelects.add(askSelect);
            }
            askDTO.setAnswerSheet(askSelects);
            askDTOS.add(askDTO);
        }
        return AjaxResult.success(askDTOS);
    }

    /**
     * @Description:
     */
    @PostMapping("/save")
    public AjaxResult save(@RequestBody List<AnswerDTO> answerDTOs)
    {
        if(CollectionUtil.isEmpty(answerDTOs)){
            return AjaxResult.success();
        }
        LoginUser loginUser = SecurityUtils.getLoginUser();
        Double score=0.00;
        for (AnswerDTO answerDTO : answerDTOs) {
            if(StringUtils.isEmpty(answerDTO.getAnswerResult())){
                return AjaxResult.error("请完善填全部答题再提交");
            }
            if(answerDTO.getAnswer().equals(answerDTO.getAnswerResult())){
                score+=1;
            }
        }
        double v =score/Double.parseDouble(answerDTOs.size() + "");
        Score scoreEntity=new Score();
        int i = (int) (v * 100.00);
        scoreEntity.setScore(i);
        scoreEntity.setUserId(loginUser.getUserId());
        scoreEntity.setUserName(loginUser.getUsername());
        scoreEntity.setCreateTime(new Date());
        scoreEntity.setType(answerDTOs.get(0).getAnswerType());
        scoreService.save(scoreEntity);
        return AjaxResult.success(scoreEntity.getScore());
    }


    /**
     * @Description:
     */
    @PostMapping("/tes")
    public AjaxResult tes(@RequestPart("file") MultipartFile file) throws IOException {
        List<Resp> memberList = EasyExcel.read(file.getInputStream())
                .head(Resp.class)
                .headRowNumber(2)
                .sheet("Sheet1")
                .doReadSync();
        List<Resp2> resp2s=new ArrayList<>();
        for (Resp member : memberList) {
            member.setPosition("["+member.getPosition()+"]");

            Resp2 resp2=new Resp2();
            BeanUtils.copyProperties(member,resp2);
//            if(!StringUtils.isEmpty(member.getActivityImg())){
//                List<String> collect = Arrays.stream(member.getActivityImg().split(",")).collect(Collectors.toList());
//                resp2.setActivityImgs(collect);
//            }
//            if(!StringUtils.isEmpty(member.getImg())){
//                List<String> collect = Arrays.stream(member.getImg().split(",")).collect(Collectors.toList());
//                for (String s : collect) {
//                    s=s.replaceAll(" ","").replace("(","").replaceAll("\\)","")
//                }
//                resp2.setImgs(collect);
//            }
//            if(!StringUtils.isEmpty(member.getExhibitsImg())){
//                List<String> collect = Arrays.stream(member.getExhibitsImg().split(",")).collect(Collectors.toList());
//                resp2.setExhibitsImgs(collect);
//            }
            if(!StringUtils.isEmpty(member.getVideo())){
                List<String> collect = Arrays.stream(member.getVideo().split(",")).collect(Collectors.toList());
                for (String s : collect) {
                    s=s.replaceAll(" ","").replace("(","").replaceAll("\\)","");
                }
                resp2.setVideos(collect);
            }
            if(!StringUtils.isEmpty(member.getActivityImg())){
                List<String> collect = Arrays.stream(member.getActivityImg().split(",")).collect(Collectors.toList());
                ArrayList<String> ls=new ArrayList<>();
                for (String s : collect) {
                    int i = s.indexOf("(");
                    if(i>=0){
                        s=s.replaceAll("\\("," (").replaceAll("png","jpg").replaceAll(" ","").replace("(","").replaceAll("\\)","");
                    }
                    ls.add(s);
                }
                resp2.setActivityImgs(ls);
            }
            if(!StringUtils.isEmpty(member.getExhibitsImg())){
                List<String> collect = Arrays.stream(member.getExhibitsImg().split(",")).collect(Collectors.toList());
                ArrayList<String> ls=new ArrayList<>();
                for (String s : collect) {
                    int i = s.indexOf("(");
                    if(i>=0){
                        s=s.replaceAll("\\("," (").replaceAll("png","jpg").replaceAll(" ","").replace("(","").replaceAll("\\)","");;
                    }
                    ls.add(s);
                }
                resp2.setExhibitsImgs(ls);
            }
            if(!StringUtils.isEmpty(member.getImg())){
                List<String> collect = Arrays.stream(member.getImg().split(",")).collect(Collectors.toList());
                ArrayList<String> ls=new ArrayList<>();
                for (String s : collect) {
                    int i = s.indexOf("(");
                    if(i>=0){
                        s=s.replaceAll("\\("," (").replaceAll("png","jpg").replaceAll(" ","").replace("(","").replaceAll("\\)","");;
                    }
                    ls.add(s);
                }
                resp2.setImgs(ls);
            }
            resp2s.add(resp2);
        }
        System.out.println(JSONUtil.toJsonStr(resp2s));
        return AjaxResult.success(123);
    }

    /**
     * @Description:
     */
    @GetMapping("/getHistory")
    public AjaxResult getHistory() throws IOException {
        LoginUser loginUser = SecurityUtils.getLoginUser();
        List<Score> list = scoreService.lambdaQuery().eq(Score::getUserId,loginUser.getUserId()).list();
        return AjaxResult.success(list);
    }

    /**
     * @Description:
     */
    @PostMapping("/importSubject")
    public AjaxResult importSubject(@RequestPart("file") MultipartFile file) throws IOException {
        List<Subject> memberList = EasyExcel.read(file.getInputStream())
                .head(Subject.class)
                .headRowNumber(1)
                .sheet("Sheet1")
                .doReadSync();
        Date date = new Date();
        for (Subject subject : memberList) {
            subject.setCreateTime(date);
        }
        subjectService.saveBatch(memberList);
        return AjaxResult.success();
    }

//     for (Subject subject : subjects) {
//        AskDTO askDTO=new AskDTO();
//        askDTO.setId(index++);
//        askDTO.setProblemType("SINGLE");
//        askDTO.setTitle(subject.getTitle());
//        List<AskSelect> askSelects=new ArrayList<>();
//        if (StringUtils.isNotEmpty(subject.getOptionA())){
//            AskSelect askSelect=new AskSelect();
//            askSelect.setAlias("A");
//            askSelect.setAnswer(subject.getOptionA());
//            askSelects.add(askSelect);
//        }
//        if (StringUtils.isNotEmpty(subject.getOptionB())){
//            AskSelect askSelect=new AskSelect();
//            askSelect.setAlias("B");
//            askSelect.setAnswer(subject.getOptionB());
//            askSelects.add(askSelect);
//        }
//        if (StringUtils.isNotEmpty(subject.getOptionC())){
//            AskSelect askSelect=new AskSelect();
//            askSelect.setAlias("C");
//            askSelect.setAnswer(subject.getOptionC());
//            askSelects.add(askSelect);
//        }
//        if (StringUtils.isNotEmpty(subject.getOptionD())){
//            AskSelect askSelect=new AskSelect();
//            askSelect.setAlias("D");
//            askSelect.setAnswer(subject.getOptionD());
//            askSelects.add(askSelect);
//        }
//        askDTO.setChildren(askSelects);
//        askDTOS.add(askDTO);
//    }
}
