package cn.source.system.controller;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.json.JSONUtil;
import cn.source.common.core.controller.BaseController;
import cn.source.common.core.domain.AjaxResult;
import cn.source.common.core.domain.model.LoginUser;
import cn.source.common.utils.SecurityUtils;
import cn.source.common.utils.StringUtils;
import cn.source.common.utils.bean.BeanUtils;
import cn.source.system.domain.entity.Score;
import cn.source.system.domain.entity.Subject;
import cn.source.system.domain.request.*;
import cn.source.system.service.IScoreService;
import cn.source.system.service.ISubjectService;
import com.alibaba.excel.EasyExcel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: 问答 api控制类
 * @author: 詹Sir
 */
@RestController
@RequestMapping("/api/common")
public class CommonApiController extends BaseController {



    /**
     * @Description:
     */
    @PostMapping("/trans")
    public AjaxResult tes(@RequestPart("file") MultipartFile file) throws IOException {
        List<Resp> memberList = EasyExcel.read(file.getInputStream())
                .head(Resp.class)
                .headRowNumber(2)
                .sheet("Sheet1")
                .doReadSync();
        List<Resp2> resp2s=new ArrayList<>();
        for (Resp member : memberList) {
            member.setPosition("["+member.getPosition()+"]");
            Resp2 resp2=new Resp2();
            BeanUtils.copyProperties(member,resp2);
            if(!StringUtils.isEmpty(member.getVideo())){
                List<String> collect = Arrays.stream(member.getVideo().split(",")).collect(Collectors.toList());
                for (String s : collect) {
                    s=s.replaceAll(" ","").replace("(","").replaceAll("\\)","");
                }
                resp2.setVideos(collect);
            }
            if(!StringUtils.isEmpty(member.getActivityImg())){
                List<String> collect = Arrays.stream(member.getActivityImg().split(",")).collect(Collectors.toList());
                ArrayList<String> ls=new ArrayList<>();
                for (String s : collect) {
                    int i = s.indexOf("(");
                    if(i>=0){
                        s=s.replaceAll("\\("," (").replaceAll(" ","").replace("(","").replaceAll("\\)","");
                    }
                    ls.add(s);
                }
                resp2.setActivityImgs(ls);
            }
            if(!StringUtils.isEmpty(member.getExhibitsImg())){
                List<String> collect = Arrays.stream(member.getExhibitsImg().split(",")).collect(Collectors.toList());
                ArrayList<String> ls=new ArrayList<>();
                for (String s : collect) {
                    int i = s.indexOf("(");
                    if(i>=0){
                        s=s.replaceAll("\\("," (").replaceAll(" ","").replace("(","").replaceAll("\\)","");;
                    }
                    ls.add(s);
                }
                resp2.setExhibitsImgs(ls);
            }
            if(!StringUtils.isEmpty(member.getImg())){
                List<String> collect = Arrays.stream(member.getImg().split(",")).collect(Collectors.toList());
                ArrayList<String> ls=new ArrayList<>();
                for (String s : collect) {
                    int i = s.indexOf("(");
                    if(i>=0){
                        s=s.replaceAll("\\("," (").replaceAll(" ","").replace("(","").replaceAll("\\)","");;
                    }
                    ls.add(s);
                }
                resp2.setImgs(ls);
            }
            resp2s.add(resp2);
        }
        System.out.println(JSONUtil.toJsonStr(resp2s));
        return AjaxResult.success(123);
    }
    /**
     * @Description:
     */
    @PostMapping("/transMobile")
    public AjaxResult transMobile(@RequestPart("file") MultipartFile file) throws IOException {
        List<Resp> memberList = EasyExcel.read(file.getInputStream())
                .head(Resp.class)
                .headRowNumber(2)
                .sheet("Sheet1")
                .doReadSync();
        List<Resp2> resp2s=new ArrayList<>();
        for (Resp member : memberList) {
            member.setPosition("["+member.getPosition()+"]");

            Resp2 resp2=new Resp2();
            BeanUtils.copyProperties(member,resp2);
            if(!StringUtils.isEmpty(member.getVideo())){
                List<String> collect = Arrays.stream(member.getVideo().split(",")).collect(Collectors.toList());
                for (String s : collect) {
                    s=s.replaceAll(" ","").replace("(","").replaceAll("\\)","");
                }
                resp2.setVideos(collect);
            }
            if(!StringUtils.isEmpty(member.getActivityImg())){
                List<String> collect = Arrays.stream(member.getActivityImg().split(",")).collect(Collectors.toList());
                ArrayList<String> ls=new ArrayList<>();
                for (String s : collect) {
                    int i = s.indexOf("(");
                    if(i>=0){
                        s=s.replaceAll("\\("," (").replaceAll(" ","").replace("(","").replaceAll("\\)","");
                    }
                    ls.add(s);
                }
                resp2.setActivityImgs(ls);
            }
            if(!StringUtils.isEmpty(member.getExhibitsImg())){
                List<String> collect = Arrays.stream(member.getExhibitsImg().split(",")).collect(Collectors.toList());
                ArrayList<String> ls=new ArrayList<>();
                for (String s : collect) {
                    int i = s.indexOf("(");
                    if(i>=0){
                        s=s.replaceAll("\\("," (").replaceAll(" ","").replace("(","").replaceAll("\\)","");;
                    }
                    ls.add(s);
                }
                resp2.setExhibitsImgs(ls);
            }
            if(!StringUtils.isEmpty(member.getImg())){
                List<String> collect = Arrays.stream(member.getImg().split(",")).collect(Collectors.toList());
                ArrayList<String> ls=new ArrayList<>();
                for (String s : collect) {
                    int i = s.indexOf("(");
                    if(i>=0){
                        s=s.replaceAll("\\("," (").replaceAll(" ","").replace("(","").replaceAll("\\)","");;
                    }
                    ls.add(s);
                }
                resp2.setImgs(ls);
            }
            if(!StringUtils.isEmpty(member.getTel())){
                List<String> collect = Arrays.stream(member.getTel().split(",")).collect(Collectors.toList());
                if(collect.size()>1){
                    resp2.setTel(collect.get(0));
                }
            }
            resp2s.add(resp2);
        }
        System.out.println(JSONUtil.toJsonStr(resp2s));
        return AjaxResult.success(123);
    }

    /**
     * @Author Pandas
     * @Date 2020/3/30 15:32
     * @Version 1.0
     * 文件名为:王大锤_171060001.jpg
     * 想转换为:171060001_王大锤.jpg
     */
    public static void main(String[] args) {
        String dirName="D:\\科普文档\\static2";
        File dir = new File(dirName);
        File[] filesList = dir.listFiles();
        // 如果路径存在且确为目录
        if (dir.exists() && dir.isDirectory()) {
            for (File file : filesList) {
                String name = file.getName().replaceAll(" ","").replace("(","").replaceAll("\\)","");
                File dest = new File(dirName+"/" +name);
                file.renameTo(dest);
            }
        }
    }
}
