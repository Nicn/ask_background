package cn.source.system.mapper;


import cn.source.system.domain.entity.Subject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * 文章管理Mapper接口
 *
 * @author 詹Sir
 * @date 2022-03-01
 */
public interface SubjectMapper extends BaseMapper<Subject>
{

    List<Subject> queryAll();

    /**
     * 查询问题
     *
     * @param id 问题主键
     * @return 问题
     */
    public Subject selectSubjectById(Long id);

    /**
     * 查询问题列表
     *
     * @param subject 问题
     * @return 问题集合
     */
    public List<Subject> selectSubjectList(Subject subject);

    /**
     * 新增问题
     *
     * @param subject 问题
     * @return 结果
     */
    public int insertSubject(Subject subject);

    /**
     * 修改问题
     *
     * @param subject 问题
     * @return 结果
     */
    public int updateSubject(Subject subject);

    /**
     * 删除问题
     *
     * @param id 问题主键
     * @return 结果
     */
    public int deleteSubjectById(Long id);

    /**
     * 批量删除问题
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSubjectByIds(Long[] ids);

}
