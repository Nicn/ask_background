package cn.source.system.mapper;


import cn.source.system.domain.entity.Score;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * Mapper接口
 *
 * @author 詹Sir
 * @date 2022-03-01
 */
public interface ScoreMapper extends BaseMapper<Score>
{
    /**
     * 查询答题记录
     *
     * @param id 答题记录主键
     * @return 答题记录
     */
    public Score selectScoreById(Long id);

    /**
     * 查询答题记录列表
     *
     * @param score 答题记录
     * @return 答题记录集合
     */
    public List<Score> selectScoreList(Score score);

    /**
     * 新增答题记录
     *
     * @param score 答题记录
     * @return 结果
     */
    public int insertScore(Score score);

    /**
     * 修改答题记录
     *
     * @param score 答题记录
     * @return 结果
     */
    public int updateScore(Score score);

    /**
     * 删除答题记录
     *
     * @param id 答题记录主键
     * @return 结果
     */
    public int deleteScoreById(Long id);

    /**
     * 批量删除答题记录
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteScoreByIds(Long[] ids);

}
