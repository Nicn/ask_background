package cn.source.system.service.impl;


import cn.source.common.utils.DateUtils;
import cn.source.system.domain.entity.Score;
import cn.source.system.mapper.ScoreMapper;
import cn.source.system.service.IScoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 友情链接Service业务层处理
 *
 * @author 詹Sir
 * @date 2022-03-12
 */
@Service
public class ScoreServiceImpl extends ServiceImpl<ScoreMapper,Score> implements IScoreService
{

    @Autowired
    private ScoreMapper scoreMapper;
    /**
     * 查询答题记录
     *
     * @param id 答题记录主键
     * @return 答题记录
     */
    @Override
    public Score selectScoreById(Long id)
    {
        return scoreMapper.selectScoreById(id);
    }

    /**
     * 查询答题记录列表
     *
     * @param score 答题记录
     * @return 答题记录
     */
    @Override
    public List<Score> selectScoreList(Score score)
    {
        return scoreMapper.selectScoreList(score);
    }

    /**
     * 新增答题记录
     *
     * @param score 答题记录
     * @return 结果
     */
    @Override
    public int insertScore(Score score)
    {
        score.setCreateTime(DateUtils.getNowDate());
        return scoreMapper.insertScore(score);
    }

    /**
     * 修改答题记录
     *
     * @param score 答题记录
     * @return 结果
     */
    @Override
    public int updateScore(Score score)
    {
        return scoreMapper.updateScore(score);
    }

    /**
     * 批量删除答题记录
     *
     * @param ids 需要删除的答题记录主键
     * @return 结果
     */
    @Override
    public int deleteScoreByIds(Long[] ids)
    {
        return scoreMapper.deleteScoreByIds(ids);
    }

    /**
     * 删除答题记录信息
     *
     * @param id 答题记录主键
     * @return 结果
     */
    @Override
    public int deleteScoreById(Long id)
    {
        return scoreMapper.deleteScoreById(id);
    }

}
