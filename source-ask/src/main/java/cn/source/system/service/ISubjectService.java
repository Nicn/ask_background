package cn.source.system.service;


import cn.source.system.domain.entity.Subject;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 友情链接Service接口
 *
 * @author 詹Sir
 * @date 2022-03-12
 */
public interface ISubjectService extends IService<Subject>
{

    List<Subject> queryAll();

    /**
     * 查询问题
     *
     * @param id 问题主键
     * @return 问题
     */
    public Subject selectSubjectById(Long id);

    /**
     * 查询问题列表
     *
     * @param subject 问题
     * @return 问题集合
     */
    public List<Subject> selectSubjectList(Subject subject);

    /**
     * 新增问题
     *
     * @param subject 问题
     * @return 结果
     */
    public int insertSubject(Subject subject);

    /**
     * 修改问题
     *
     * @param subject 问题
     * @return 结果
     */
    public int updateSubject(Subject subject);

    /**
     * 批量删除问题
     *
     * @param ids 需要删除的问题主键集合
     * @return 结果
     */
    public int deleteSubjectByIds(Long[] ids);

    /**
     * 删除问题信息
     *
     * @param id 问题主键
     * @return 结果
     */
    public int deleteSubjectById(Long id);

}
