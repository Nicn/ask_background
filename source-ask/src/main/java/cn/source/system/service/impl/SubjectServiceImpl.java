package cn.source.system.service.impl;


import cn.source.system.domain.entity.Subject;
import cn.source.system.mapper.SubjectMapper;
import cn.source.system.service.ISubjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 友情链接Service业务层处理
 *
 * @author 詹Sir
 * @date 2022-03-12
 */
@Service
public class SubjectServiceImpl extends ServiceImpl<SubjectMapper, Subject> implements ISubjectService
{
    @Autowired
    private SubjectMapper subjectMapper;


    @Override
    public List<Subject> queryAll() {
        return  subjectMapper.queryAll();
    }

    /**
     * 查询问题
     *
     * @param id 问题主键
     * @return 问题
     */
    @Override
    public Subject selectSubjectById(Long id)
    {
        return subjectMapper.selectSubjectById(id);
    }

    /**
     * 查询问题列表
     *
     * @param subject 问题
     * @return 问题
     */
    @Override
    public List<Subject> selectSubjectList(Subject subject)
    {
        return subjectMapper.selectSubjectList(subject);
    }

    /**
     * 新增问题
     *
     * @param subject 问题
     * @return 结果
     */
    @Override
    public int insertSubject(Subject subject)
    {
        return subjectMapper.insertSubject(subject);
    }

    /**
     * 修改问题
     *
     * @param subject 问题
     * @return 结果
     */
    @Override
    public int updateSubject(Subject subject)
    {
        return subjectMapper.updateSubject(subject);
    }

    /**
     * 批量删除问题
     *
     * @param ids 需要删除的问题主键
     * @return 结果
     */
    @Override
    public int deleteSubjectByIds(Long[] ids)
    {
        return subjectMapper.deleteSubjectByIds(ids);
    }

    /**
     * 删除问题信息
     *
     * @param id 问题主键
     * @return 结果
     */
    @Override
    public int deleteSubjectById(Long id)
    {
        return subjectMapper.deleteSubjectById(id);
    }
}
