package cn.source.system.service;


import cn.source.system.domain.entity.Score;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 友情链接Service接口
 *
 * @author 詹Sir
 * @date 2022-03-12
 */
public interface IScoreService  extends IService<Score>
{
    /**
     * 查询答题记录
     *
     * @param id 答题记录主键
     * @return 答题记录
     */
    public Score selectScoreById(Long id);

    /**
     * 查询答题记录列表
     *
     * @param score 答题记录
     * @return 答题记录集合
     */
    public List<Score> selectScoreList(Score score);

    /**
     * 新增答题记录
     *
     * @param score 答题记录
     * @return 结果
     */
    public int insertScore(Score score);

    /**
     * 修改答题记录
     *
     * @param score 答题记录
     * @return 结果
     */
    public int updateScore(Score score);

    /**
     * 批量删除答题记录
     *
     * @param ids 需要删除的答题记录主键集合
     * @return 结果
     */
    public int deleteScoreByIds(Long[] ids);

    /**
     * 删除答题记录信息
     *
     * @param id 答题记录主键
     * @return 结果
     */
    public int deleteScoreById(Long id);


}
