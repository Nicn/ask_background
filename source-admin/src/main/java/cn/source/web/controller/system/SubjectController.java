package cn.source.web.controller.system;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import cn.source.system.domain.entity.Subject;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.source.common.annotation.Log;
import cn.source.common.core.controller.BaseController;
import cn.source.common.core.domain.AjaxResult;
import cn.source.common.enums.BusinessType;
import cn.source.system.service.ISubjectService;
import cn.source.common.utils.poi.ExcelUtil;
import cn.source.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 问题Controller
 *
 * @author sourcebyte.vip
 * @date 2023-08-27
 */
@RestController
@RequestMapping("/system/subject")
public class SubjectController extends BaseController
{
    @Autowired
    private ISubjectService subjectService;

    /**
     * 查询问题列表
     */
    @PreAuthorize("@ss.hasPermi('system:subject:list')")
    @GetMapping("/list")
    public TableDataInfo list(Subject subject)
    {
        startPage();
        List<Subject> list = subjectService.selectSubjectList(subject);
        return getDataTable(list);
    }

    /**
     * 导出问题列表
     */
    @PreAuthorize("@ss.hasPermi('system:subject:export')")
    @Log(title = "问题", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Subject subject)
    {
        List<Subject> list = subjectService.selectSubjectList(subject);
        ExcelUtil<Subject> util = new ExcelUtil<Subject>(Subject.class);
        util.exportExcel(response, list, "问题数据");
    }

    /**
     * 获取问题详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:subject:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(subjectService.selectSubjectById(id));
    }

    /**
     * 新增问题
     */
    @PreAuthorize("@ss.hasPermi('system:subject:add')")
    @Log(title = "问题", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Subject subject)
    {
        return toAjax(subjectService.insertSubject(subject));
    }

    /**
     * 修改问题
     */
    @PreAuthorize("@ss.hasPermi('system:subject:edit')")
    @Log(title = "问题", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Subject subject)
    {
        return toAjax(subjectService.updateSubject(subject));
    }

    /**
     * 题库管理
     */
    @Log(title = "题库管理", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('system:subject:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<Subject> util = new ExcelUtil<Subject>(Subject.class);
        List<Subject> houseList = util.importExcel(file.getInputStream());
        Date date = new Date();
        for (Subject subject : houseList) {
            subject.setCreateTime(date);
        }
        subjectService.saveBatch(houseList);
        return AjaxResult.success("导入成功");
    }
    /**
     * 导出题库详情列表
     */
    @PreAuthorize("@ss.hasPermi('system:subject:export')")
    @Log(title = "题库模板", businessType = BusinessType.EXPORT)
    @PostMapping("/templete")
    public void templete(HttpServletResponse response, Subject houseRoom)
    {
        ExcelUtil<Subject> util = new ExcelUtil<Subject>(Subject.class);
        util.exportExcel(response, null, "题库模板");
    }

    /**
     * 删除问题
     */
    @PreAuthorize("@ss.hasPermi('system:subject:remove')")
    @Log(title = "问题", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(subjectService.deleteSubjectByIds(ids));
    }
}
